# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php8-pecl-zstd
_extname=zstd
pkgver=0.12.2
pkgrel=0
pkgdesc="PHP 8.0 Zstandard extension - PECL"
url="https://pecl.php.net/package/zstd"
arch="all"
license="MIT"
depends="php8-common"
makedepends="php8-dev php8-pecl-apcu zstd-dev"
checkdepends="php8-openssl"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir/$_extname-$pkgver"

build() {
	phpize8
	./configure --prefix=/usr --with-php-config=php-config8 --with-libzstd
	make
}

check() {
	local _modules=/usr/lib/php8/modules
	make NO_INTERACTION=1 REPORT_EXIT_STATUS=1 test PHP_TEST_SHARED_EXTENSIONS=" \
		-d extension=$_modules/apcu.so \
		-d extension=$_modules/openssl.so \
		-d extension=modules/$_extname.so" TESTS=--show-diff
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/php8/conf.d
	install -d $_confdir
	echo "extension=$_extname" > $_confdir/$_extname.ini
}

sha512sums="
b628851e54815bf3f9cfd54b9e161890e77b7fadb4defc1b65611a4300a38844b6b9bbe90aeef183a9d9e4ef231447197f0a8a576ca85bff050f63765feeb2d8  php-pecl-zstd-0.12.2.tgz
"
